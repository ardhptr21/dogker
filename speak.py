#/usr/bin/env python3

import argparse
import json
import sys
import os

path = os.path.dirname(os.path.realpath(__file__))
lang = json.load(open(path + '/lang.json', 'r'))

parser = argparse.ArgumentParser(
    usage="./speak.py speak.py hello",
    description="Speak a text to Woof 🐕",
)

parser.add_argument("text", help="Text to speak")
parser.add_argument("delimiter", help="Delimiter each Woof", default=" ", nargs='?')

args = parser.parse_args()

text = args.text
delimiter = args.delimiter

speakwof = map(lambda x: lang[x], text)
speakwof = delimiter.join(speakwof) + '\n'

sys.stdout.write(speakwof)