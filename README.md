# DogKer

![DogKer](assets/dogker.gif)

## What about DogKer? 🐕
DogKer, seekor anjing pelacak terbaik, tanpa sengaja menjadi hacker handal setelah menemukan laptop majikannya yang terbuka. Namun, ia memutuskan untuk berhenti dan menggunakan kemampuan teknologinya untuk membantu orang lain sebagai ahli keamanan siber dan membuka layanan pelatihan. Akhirnya, DogKer menemukan kebahagiaan sejati sebagai pahlawan dunia maya yang berintegritas tinggi.


## DogKer Speak Tool ⚒️
DogKer juga membuat sebuah tools yang bisa kamu gunakan untuk berkomunikasi dengan lebih aman. Tools ini bernama **DogKer Speak**, dengan tools ini kamu bisa mengkonversi pesan rahasia kamu menjadi pesan yang bisa dibaca oleh DogKer.

### How to use it?
```bash
$ python3 speak.py <text> <delimiter>
```
### Example

![DogKer Speak](assets/tutorial.gif)

## Thank You 🙏