FROM python:alpine

RUN mkdir /app
COPY . /app

RUN chmod +x /app/speak.py
RUN apk add --no-cache git

WORKDIR /

ENTRYPOINT [ "python", "/app/speak.py" ]